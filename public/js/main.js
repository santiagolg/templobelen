//Llama la funcion init cuando carga la pagina
window.onload = inicio;
// Creacion de las variables
var menu,botones, secciones, barraMenu, correoRegistro, contrasenaRegistro,correoIngreso, contrasenaIngreso, usuarioIngresado, listaCursos, cursosRegistro,
 palabraSeccion, seccion, cursosPersona, referenciaSeccion, formularioInicio,formularioRegistro, menuMovil, estadoMenu, configuracion,
 codigoError, mensajeError, baseDatos, codigoUsuario;
//Primera funcion que se ejecuta
function inicio() {
	inicializacionVariables();
	inicializacionEventos();
}

//funcion que inicializa todas las variables necesarias
function inicializacionVariables(){
	// Initialize Firebase
  	configuracion = {
	    apiKey: "AIzaSyBeJehx5_EGM3oMefS4uj3Ue4riTaFlJzc",
	    authDomain: "templo-belen.firebaseapp.com",
	    databaseURL: "https://templo-belen.firebaseio.com",
	    projectId: "templo-belen",
	    storageBucket: "",
	    messagingSenderId: "625344605835"
  	};
  	//Inicializamos el Firebase
  	firebase.initializeApp(configuracion);
  	//ReferenciaSeccion a la base de datos
  	baseDatos = firebase.database();
  	//Diferentes array que se usaran
	botones = [];
	listaCursos = [];
	cursosRegistro = [];
	cursosVistos = [];
	//array de los botones usados para la navegacion
	botones[0] = document.getElementById("seccion_inicio");
	botones[1] = document.getElementById("seccion_ingreso");
	botones[2] = document.getElementById("seccion_calendario");
	botones[3] = document.getElementById("seccion_cursos");
	botones[4] = document.getElementById("seccion_registro");
	botones[5] = document.getElementById("volver_ingreso");
	botones[6] = document.getElementById("ir_cursos");
	botones[7] = document.getElementById("ir_calendario");
	botones[8] = document.getElementById("salir_inicio");
	//Estado del menu
	estadoMenu = false;
	//botones del menu
	menu = document.getElementById("menu");
	menuMovil = document.getElementById("menu_desplegable");
	barraMenu = document.getElementById("barra_menu");
	//Campos del registro
	formularioRegistro = document.getElementById("form_registro");
	correoRegistro = document.getElementById("correo");
	contrasenaRegistro = document.getElementById("contrasena");
	cursosPersona = [];
	//Campos ingreso
	codigoUsuario = "";
	formularioInicio = document.getElementById("form_ingreso");
	correoIngreso = document.getElementById("correoIngreso");
	contrasenaIngreso = document.getElementById("contrasenaIngreso");
	//Array de los cursos seleccionados en el registro
	cursosRegistro[0] = document.getElementById("curso_creemos");
	cursosRegistro[1] = document.getElementById("curso_lideres");
	cursosRegistro[2] = document.getElementById("curso_padres");
	//Array de los cursos
	listaCursos[0] = document.getElementById("creemos");
	listaCursos[1] = document.getElementById("lideres");
	listaCursos[2] = document.getElementById("padres");
	//variables para la funcion procesar click
	palabraSeccion = "";
	seccion = "";
	//secciones de la pagina web
	secciones = [];
	secciones[0] = document.getElementById("inicio");
	secciones[1] = document.getElementById("ingreso");
	secciones[2] = document.getElementById("calendario");
	secciones[3] = document.getElementById("cursos");
	secciones[4] = document.getElementById("registro");
}

//funcion que inicializa los eventos de los botones
function inicializacionEventos(){
	formularioRegistro.addEventListener("submit",ingresarRegistro);
	formularioInicio.addEventListener("submit",ingresarUsuario);
	menuMovil.addEventListener("click",desplegarMenu);
	for(var i in botones)
	{
		botones[i].addEventListener("click",procesarClick);
	}

}

//Salida del usuario
function salirUsuario(evt){
	firebase.auth().signOut().then(function() {
	  // Sign-out successful.
	  usuarioIngresado = null;
	  alert("Salida con éxito");
	  cambiarMenu();
	  reiniciarCursos();
	}).catch(function(error) {
	  // An error happened.
	});	
}

function reiniciarCursos(){
	for (var i in listaCursos) {
		listaCursos[i].className = "list-group-item";
	}
}

//funcion que valida que los campos de registro no esten vacios e ingresa la persona en el localstorage
function ingresarRegistro(evt){
	firebase.auth().createUserWithEmailAndPassword(correoRegistro.value, contrasenaRegistro.value).then(function(){
		registrarCursosRegistro();
		verificarCorreo();
		formularioRegistro.reset();
		secciones[4].className = "ocultar";
		secciones[1].className = "animated fadeIn";
	}).catch(function(error) {
	  // Handle Errors here.
	  codigoError = error.code;
	  mensajeError = error.message;
	  alert("No se pudo registrar el usuario\n"+mensajeError);
	  //
	});
	

}
//funcion que mira que cursos eligio el usuario en el registro
function registrarCursosRegistro(evt){
		for (var i in cursosRegistro) {
			if(cursosRegistro[i].checked){
					cursosPersona.push(cursosRegistro[i].value);
			}
		}
		baseDatos.ref('cursos/' + firebase.auth().currentUser.uid).set({
		  email: correoRegistro.value,
		  cursos: cursosPersona
		});
}

//funcion que envia un correo de verificacion al momento de registrarse
 function verificarCorreo() {
      // [START sendemailverification]
      firebase.auth().currentUser.sendEmailVerification().then(function() {
        alert('Correo de verificación enviado');
      });
    }

//funcion que valida que los campos de ingreso no esten vacios y que la persona se encuentre en el localstorage
function ingresarUsuario(evt){
		firebase.auth().signInWithEmailAndPassword(correoIngreso.value, contrasenaIngreso.value).then(function(){
			if(firebase.auth().currentUser.emailVerified){
				codigoUsuario = firebase.auth().currentUser.uid;
				firebase.database().ref('/cursos/' + codigoUsuario).once('value').then(function(snapshot) {
					usuarioIngresado = snapshot.val();
					alert("¡Usuario Ingresado!\n"+usuarioIngresado.email);
					llenarCursos();
				});
				formularioInicio.reset();
				secciones[1].className = "ocultar";
				secciones[0].className = "animated fadeIn";
				cambiarMenu();
				botones[0].className = "ocultar";
				barraMenu.className = "navbar navbar-inverse navbar-fixed-top absoluto fondo-menu";
			}else{
				firebase.auth().signOut().then(function() {
				  // Sign-out successful.
				  usuarioIngresado = null;
				  alert("Verifica tu correo electrónico");
				}).catch(function(error) {
				  // An error happened.
				});	
			}
		}).catch(function(error) {
		  // Handle Errors here.
		  codigoError = error.code;
		  mensajeError = error.message;
		  alert("Error en la autenticación" + mensajeError);
		  //
		});
}

//cambia el menu cuando el usuario ingresa o cuando sale
function cambiarMenu(evt){
	if(botones[8].className == "btn btn-link ocultar"){
		botones[1].className = "btn btn-link ocultar";
		botones[8].className = "btn btn-link";
	}else{
		botones[1].className = "btn btn-link";
		botones[8].className = "btn btn-link ocultar";
	}

}


//Despliga u oculta el menu en los dispositivos moviles
function desplegarMenu(evt) {
	if(estadoMenu == false && this.id == "menu_desplegable"){
		menu.className = "mostrar navbar-toggle collapsed animated fadeInDown";
		
		estadoMenu = true;
	}
	else if (estadoMenu == true){
	menu.className = "ocultar navbar-toggle collapsed animated fadeOut";
		estadoMenu = false;
	}	
}

//Llena los cursos que el usuario ingresado ya vio
function llenarCursos(evt){
	for (var i in listaCursos) {
		for (var j in usuarioIngresado.cursos) {
			if (listaCursos[i].id == usuarioIngresado.cursos[j]){
				listaCursos[i].className = "list-group-item list-group-item-success";
			}
		}
	}
}

//Funcion que permite la navegacion entre las diferentes secciones
function procesarClick(evt)
{
	palabraSeccion = this.id;
	seccion = palabraSeccion.split("_")[1];
	if (palabraSeccion	== "salir_inicio") {
		salirUsuario();
	}
	if(seccion == "inicio"){
		botones[0].className = "ocultar";
		barraMenu.className = "navbar navbar-inverse navbar-fixed-top absoluto fondo-menu";
	}else{
		botones[0].className = "btn btn-link";
		barraMenu.className = "navbar navbar-inverse navbar-fixed-top relativo fondo-menu";
	}
	referenciaSeccion = document.getElementById(seccion);
	ocultar();
	referenciaSeccion.className = "animated fadeIn";
	desplegarMenu();

}

//oculta todas las secciones
function ocultar()
{
	for(var i in secciones)
	{
		secciones[i].className = "ocultar";
	}
}